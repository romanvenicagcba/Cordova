var ideas = [];

function traerIdeas(a,b){
    if (ideas.length == 0){
        document.getElementById(b).style.display = "block";
    }else{
        document.getElementById(b).style.display = "none";
        document.getElementById(a).innerHTML = "";
        for(i=0; i<ideas.length; i++)
            document.getElementById(a).innerHTML += ideas[i].titulo + '</br>';}
}

function crearIdea(a,b){
    document.getElementById(a).style.display = "none";
    document.getElementById(b).style.display = "block";
}

function subirIdea(a,b,c,d){
    var titulo = document.getElementById(c).value;
    var descripcion = document.getElementById(d).value;
    console.log(titulo)
    console.log(descripcion)
    ideas.push({titulo: titulo, descripcion: descripcion});
    document.getElementById(a).style.display = "none";
    document.getElementById(b).style.display = "block";
    document.getElementById(d).value = document.getElementById(c).value = "";
}

function sinIdeasPres(a,b){
    document.getElementById(a).style.display="none";
    document.getElementById(b).style.display="block";
}

function sinIdeasLev(a,b){
    document.getElementById(a).style.display="block";
    document.getElementById(b).style.display="none";
}
